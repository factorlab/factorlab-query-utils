# Databricks notebook source
import datetime as dt
print("FL-data update process start...");
start_time = dt.datetime.today()
print(start_time)

# COMMAND ----------

# MAGIC %run /Shared/Production/factorlab-data-utils/factorlab-db-config

# COMMAND ----------

# DBTITLE 1,Properties:
# # Factorlab Data source:
# from datetime import date, datetime, timedelta

# spark.conf.set("spark.sql.legacy.parquet.int96RebaseModeInWrite", "CORRECTED")
# jdbcHostname = "proddbinstance21.cmjey1fo7ccx.us-east-1.rds.amazonaws.com"
# jdbcDatabase = "factorlab"
# jdbcPort = 3306
# # jdbcUser= "databricks_user"
# # jdbcPass= "winter7821(cant"
# jdbcUser = dbutils.secrets.get(scope = "prod-secrets", key = "rds.username")
# jdbcPass = dbutils.secrets.get(scope = "prod-secrets", key = "rds.password")
# jdbcUrl = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(jdbcHostname, jdbcPort, jdbcDatabase, jdbcUser, jdbcPass)

# schemaName = "";
# DROP_TABLE_STR = "DROP TABLE IF EXISTS " + schemaName;

# #Zone tables:
# zone_table_list = ["zone", "zone_configuration", "fl_tag_lookup", "fl_tag_config"]
# # list of tables for observation:
# observations_table_list =  ["observation_session", "observation", "observation_session_media", "observation_media_transcript", "obs_transcript_model_score","observation_media_details", "media_pipeline_event", "media_pipeline_log"]
# observations_ar_table_list =  ["observation_session", "observation", "observation_session_media"]
# # list of tables for users
# users_table_list = ["app_role", "app_user", "party", "person", "sales_person", "app_user_roles", "user_position", "party_regions"]
# # list of tables for entity tables
# entity_table_list = ["account", "account_location", "opportunity", "opportunity_record_type", "thing", "iot_device", "user_activity_log"]
# # list of tables for observations signals tables
# signals_table_list = ["app_signal", "signal_related_records", "app_signal_like", "app_signal_comment"]
# signals_ar_table_list = ["app_signal", "signal_related_records", "app_signal_like", "app_signal_comment"]
# # list of phenomenon and phenomenon_group tables:
# pg_table_list = ["phenomenon_group", "phenomenon"]
# # list of iot related tables:
# iot_tables_list = ["opportunity_record_type", "thing", "iot_device", "watch_fl_key", "watch_events_history", "watch_events_summary", "watch_status_history", "watch_events_log"]
# table_with_zones=["observation_session", "observation_session_ar", "observation", "observation_ar", "observation_media_transcript", "obs_transcript_model_score", "observation_media_details", "app_role", "party", "user_position", "account_location", "account", "opportunity", "opportunity_record_type", "thing", "iot_device", "user_activity_log", "signal_related_records", "signal_related_records_ar", "phenomenon_group"]
# factorlab_zone_ids = ""
# factorlab_zone_list = []
# watch_zone_ids = []

# COMMAND ----------

# df = spark.read.jdbc(url=jdbcUrl, table="user_activity_log")
# df.write.format("delta").option("overwriteSchema", "true").mode("overwrite").saveAsTable(schemaName + "user_activity_log_temp")

# COMMAND ----------

# DBTITLE 1,Utils Functions:
def get_update_fl_data(baseTable, newTable):
  #spark.sql(DROP_TABLE_STR + tablename)
  if baseTable in table_with_zones:
    df = spark.read.jdbc(url=jdbcUrl, table="(select * from {0} where zone in {1}) as {2}".format(baseTable, factorlab_zone_ids , baseTable))
    df.write.format("delta").option("overwriteSchema", "true").mode("overwrite").saveAsTable(schemaName + newTable)
  else:
    df = spark.read.jdbc(url=jdbcUrl, table=baseTable)
    df.write.format("delta").option("overwriteSchema", "true").mode("overwrite").saveAsTable(schemaName + newTable)
  
def get_append_fl_data(baseTable, newTable):
  #spark.sql(DROP_TABLE_STR + tablename)
  if baseTable in table_with_zones:
    df = spark.read.jdbc(url=jdbcUrl, table="(select * from {0} where zone in {1}) as {2}".format(baseTable, factorlab_zone_ids , baseTable))
    df.write.format("delta").option("overwriteSchema", "true").mode("append").saveAsTable(schemaName + newTable)
  else:
    df = spark.read.jdbc(url=jdbcUrl, table=baseTable)
    df.write.format("delta").option("overwriteSchema", "true").mode("append").saveAsTable(schemaName + newTable)
    
def get_update_fl_data_frame(sqlQuery, viewName):
  #spark.sql(DROP_TABLE_STR + viewName)
  df_fl = spark.sql(sqlQuery.replace("[db].", schemaName))
  df_fl.write.format("delta").option("overwriteSchema", "true").mode("overwrite").saveAsTable(schemaName + viewName)
  
def get_watch_iot_zones():
  df = spark.sql("select id from zone where mode = 'Safety_Coach_Iot' order by id desc")
  for row in df.rdd.collect():
    watch_zone_ids.append(row.id)

def get_safety_zones():
  df = spark.sql("select distinct zone from zone_configuration where param_name = 'exportToDatabricks' order by zone desc")
  for row in df.rdd.collect():
    factorlab_zone_list.append(str(row.zone))
  
  

# COMMAND ----------

# DBTITLE 1,Get Safety and Watch Zones:

# Export Zone realted tables:
for tablename in zone_table_list:
   get_update_fl_data(tablename, tablename)

# Get databricks enabled zone only
factorlab_zone_ids = ""
factorlab_zone_list = []
watch_zone_ids = []
get_safety_zones()
get_watch_iot_zones()

factorlab_zone_ids = '(' + ','.join(factorlab_zone_list) + ')';
print(factorlab_zone_ids)


# COMMAND ----------

# DBTITLE 1,Delete And Exports Users Tables:
# Export users realted tables:
for tablename in users_table_list:
   get_update_fl_data(tablename, tablename)

# COMMAND ----------

# DBTITLE 1,Delete And Export Observation Tables:
# Export observation realted tables:
for tablename in observations_table_list:
   get_update_fl_data(tablename, tablename)

# Export observation archieve realted tables:
for tablename in observations_ar_table_list:
  get_append_fl_data(tablename + "_ar", tablename)    

# COMMAND ----------

# DBTITLE 1,Delete And Exports Entity Tables:
# Export entity realted tables:
for tablename in entity_table_list:
   get_update_fl_data(tablename, tablename)

# COMMAND ----------

# DBTITLE 1,Delete And Exports Signals Tables:
# Export signals data from Factorlab:
for tablename in signals_table_list:
  get_update_fl_data(tablename, tablename)
  
# Export signals archiev data from Factorlab:
for tablename in signals_ar_table_list:
  get_append_fl_data(tablename + "_ar", tablename)

# COMMAND ----------

# DBTITLE 1,Delete And Exports Phenomenon/Phenomenon group Tables:
# Export phenomenon/phenomenon group realted tables:
for tablename in pg_table_list:
   get_update_fl_data(tablename, tablename)

# COMMAND ----------

# DBTITLE 1,Delete And Exports Iot Tables:
# Export iot related tables:
for tablename in iot_tables_list:
  get_update_fl_data(tablename, tablename)

# COMMAND ----------

# DBTITLE 1,Delete And Create User summary View Table:
#Create Users summary View Table:
userSummaryView = "Safety_Obs_Users"
sqlString = "SELECT  party.id as user_id, party.dtype as user_dtype, party.entity_state, "
sqlString += "party.email, party.zone, party.external_id,  party.name, party.time_zone, party.create_date," 
sqlString += "app_user.username, person.first_name, person.middle_name, " 
sqlString += "person.last_name, sales_person.url, sales_person.thumbnail_url, app_role.id as role_id, " 
sqlString += "app_role.name as role_name, user_position.id 	position_id, " 
sqlString += "user_position.name as position_name, region.id as region_id, region.name as region_name " 
sqlString += "FROM [db].party  LEFT JOIN [db].app_user ON party.id = app_user.party " 
sqlString += "LEFT JOIN [db].person ON person.party = party.id " 
sqlString += "LEFT JOIN [db].sales_person ON sales_person.party = party.id " 
sqlString += "LEFT JOIN [db].app_user_roles roles ON roles.app_user = party.id " 
sqlString += "LEFT JOIN [db].app_role ON app_role.id = roles.roles " 
sqlString += "LEFT JOIN [db].user_position on user_position.id = person.job_position " 
sqlString += "LEFT JOIN [db].party_regions ON party_regions.party = party.id " 
sqlString += "LEFT JOIN [db].party region ON region.id = party_regions.regions AND region.dtype = 'Region' " 
sqlString += "where party.zone in " + factorlab_zone_ids + " AND party.dtype in ('SalesPerson', 'Contact', 'Contractor') " 
sqlString += "AND party.entity_state != 'DELETE'"

get_update_fl_data_frame(sqlString, userSummaryView)

# COMMAND ----------

# DBTITLE 1,Delete and Create Signals Likes Table:
#Create Observation/Signals Likes View Table:
signalsLikeView = "Safety_Obs_Likes"
likeSqlQuery ="SELECT os.id as obs_session_id, observer.party as observer_id, account.id as project_id, subject.id as subject_id, subject.name as subject_name, "
likeSqlQuery += "os.zone as zone, signal_like.id as like_id, signal_like.like_date as like_date, like_party.id as liked_by_id, "								
likeSqlQuery += " like_party.name as liked_by_name FROM [db].observation_session os "
likeSqlQuery += "INNER JOIN [db].signal_related_records srr ON srr.zone = os.zone AND srr.obs_session_id = os.id " 
likeSqlQuery += "INNER JOIN [db].app_signal_like signal_like ON signal_like.app_signal = srr.app_signal "					
likeSqlQuery += "INNER JOIN [db].party like_party ON signal_like.liked_by = like_party.id AND like_party.dtype = 'SalesPerson' "
likeSqlQuery += "AND (isnull(like_party.deleted) or like_party.deleted = 0) " 
likeSqlQuery += "INNER JOIN [db].person observer ON os.primary_observer = observer.party "
likeSqlQuery += "INNER JOIN [db].app_user observer_user ON os.primary_observer = observer_user.party "
likeSqlQuery += "LEFT JOIN [db].account account ON os.account = account.id "
likeSqlQuery += "LEFT JOIN [db].party subject ON os.primary_subject = subject.id AND (isnull(subject.deleted) or subject.deleted = 0) "
likeSqlQuery += "AND subject.dtype in ('Contact', 'SalesPerson') "
likeSqlQuery += "INNER JOIN [db].zone zo ON zo.id = os.zone "
likeSqlQuery += "WHERE  os.deleted = 0 and os.zone in " + factorlab_zone_ids;

get_update_fl_data_frame(likeSqlQuery, signalsLikeView)

# COMMAND ----------

# DBTITLE 1,Delete and Create Signals Comments Table:
#Create Observation/Signals omments View Table:
signalsCommentView = "Safety_Obs_Comments"
commentSqlQuery = "SELECT os.id as obs_session_id, observer.party as observer_id, account.id as project_id, "
commentSqlQuery += "os.zone as zone, comment.id as comment_id, comment.comment as comment_text,  comment_party.id as comment_posted_by_id, "								
commentSqlQuery += "comment_party.name as comment_posted_by_name, comment.media_type as comment_media_type, comment.thumbnail_url as comment_thumbnail_url, "	
commentSqlQuery += "comment.url as comment_media_url, comment.date_posted as comment_date_posted,  "
commentSqlQuery += "subject.id as subject_id, subject.name as subject_name "		
commentSqlQuery += " FROM [db].observation_session os "
commentSqlQuery += "INNER JOIN [db].signal_related_records srr ON srr.zone = os.zone AND srr.obs_session_id = os.id " 
commentSqlQuery += "INNER JOIN [db].app_signal_comment comment ON comment.app_signal = srr.app_signal "					
commentSqlQuery += "INNER JOIN [db].party comment_party ON comment.posted_by = comment_party.id AND comment_party.dtype = 'SalesPerson' "
commentSqlQuery += "AND (isnull(comment_party.deleted) or comment_party.deleted = 0) " 
commentSqlQuery += "INNER JOIN [db].person observer ON os.primary_observer = observer.party "
commentSqlQuery += "INNER JOIN [db].app_user observer_user ON os.primary_observer = observer_user.party "
commentSqlQuery += "LEFT JOIN [db].account account ON os.account = account.id "
commentSqlQuery += "LEFT JOIN [db].party subject ON os.primary_subject = subject.id AND (isnull(subject.deleted) or subject.deleted = 0) "
commentSqlQuery += "AND subject.dtype in ('Contact', 'SalesPerson') "
commentSqlQuery += "INNER JOIN [db].zone zo ON zo.id = os.zone "
commentSqlQuery += "WHERE  os.deleted = 0 and os.zone in " + factorlab_zone_ids;

get_update_fl_data_frame(commentSqlQuery, signalsCommentView)

# COMMAND ----------

# DBTITLE 1,Delete and Create Observation Summary/Details/Video scores Table (Not used anymore):
# #Create Observation details with video score View Table:
# observationDetailsView = "Safety_Obs_Video"
# obsSqlQuery = "select os.id as obs_session_id, zo.id as zone, os.dtype as d_type, os.category as category, os.open_issue as open_issue, "
# obsSqlQuery += "date_format(os.session_date, 'yyyy-MM-dd')       as  session_date, "
# obsSqlQuery += "date_format(os.session_time, 'HH:mm:ss')      as  session_time, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM-dd'), ' ', date_format(os.session_time, 'HH:mm:ss')) as session_datetime, "
# obsSqlQuery += "weekofyear(os.session_date)       as  session_year_week, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy'), '-01-01')   as   session_year, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM'), '-01') as session_year_month, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM-dd'), ' ', date_format(os.session_time, 'HH'), ':00:00') as session_year_hour, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM-dd'),' ',date_format(os.session_time, 'HH:mm'), ':00') as session_year_minute, "
# obsSqlQuery += "observer_user.region_id_tokens        as observer_region, "
# obsSqlQuery += "observer_user.username                as observer_username, "
# obsSqlQuery += "observer_party.id 					  as observer_id, "
# obsSqlQuery += "observer_party.time_zone			  as observer_time_zone, "
# obsSqlQuery += "observer_party.name					  as observer_name, "
# obsSqlQuery += "observer.first_name as observer_first_name, "
# obsSqlQuery += "TRIM(concat(IFNULL(observer.middle_name, ''), ' ' , observer.last_name))  as observer_last_name, "
# obsSqlQuery += "observer_user.role_name_tokens       as observer_role_names, "
# obsSqlQuery += "observer_position.id                 as observer_position_id, "
# obsSqlQuery += "observer_position.name               as observer_position_name, "
# obsSqlQuery += "subject_user.region_id_tokens        as  subject_region, "
# obsSqlQuery += "subject.first_name  as subject_first_name, "
# obsSqlQuery += "subject.last_name as subject_last_name,  subject.party as subject_id, "
# obsSqlQuery += "subject_user.role_name_tokens       as subject_role_names, "
# obsSqlQuery += "subject_position.id                 as subject_position_id, "
# obsSqlQuery += "subject_position.name               as subject_position_name, "
# obsSqlQuery += "opportunity.name as opportunity_name, "
# obsSqlQuery += "opportunity.close_date              as  opportunity_close_date, "
# obsSqlQuery += "record_type.name as opportunity_record_type_name, "
# obsSqlQuery += "account.name as       account_name, "
# obsSqlQuery += "account.name as       account_id, account.entity_state as account_entity_state,"
# obsSqlQuery += "account.time_zone as  account_time_zone, "
# obsSqlQuery += "account.location as account_location, "
# obsSqlQuery += "account_region.id					as account_region_id, "
# obsSqlQuery += "account_region.name					as account_region_name, "
# obsSqlQuery += "os.latitude as latitude, "
# obsSqlQuery += "os.longitude as longitude, "
# obsSqlQuery += "os.accuracy as location_accuracy, "
# obsSqlQuery += "ob.obs_value as       obs_value, "
# obsSqlQuery += "ob.comments   as     free_text, "
# obsSqlQuery += "pg.name as phenomenon_group_name, "
# obsSqlQuery += "ph.name as phenomenon_name, "
# obsSqlQuery += "ph.statement as phenomenon_statement, "
# obsSqlQuery += "ph.dtype as phenomenon_type, "
# obsSqlQuery += "ph.units as phenomenon_units, "
# obsSqlQuery += "ph.high_end_text as phenomenon_high_end_text, "
# obsSqlQuery += "ph.low_end_text as phenomenon_low_end_text, "
# obsSqlQuery += "ph.multiple as phenomenon_multiple, "
# obsSqlQuery += "ph.auto_fill_key as auto_fill_key, "
# obsSqlQuery += "(case "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 26  then ph.low_end_text "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 51  then 'Two Severity' "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 76  then 'Three Severity' "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 101 then ph.high_end_text "
# obsSqlQuery += " end ) as obs_value_text, "      
# obsSqlQuery += "os.comments as  comments, "
# obsSqlQuery += "ob.display_value as display_value, "
# obsSqlQuery += "ob.url as  ob_url, "
# obsSqlQuery += "ob.thumbnail_url as ob_thumbnail_url, "
# obsSqlQuery += "ob.media_type as ob_media_type, "
# obsSqlQuery += "ob.answer_feedback as answer_feedback, "
# obsSqlQuery += "ob.ph_type as  ph_type, "
# obsSqlQuery += "related_thing.name as related_thing_name, "
# obsSqlQuery += "related_thing.display_name      as        related_thing_display_name, "
# obsSqlQuery += "iot_device.id as   device_id, "
# obsSqlQuery += "iot_device.name as  device_name, "
# obsSqlQuery += "os.hash_tag_strings 					as	hash_tag_strings, "
# obsSqlQuery += "os.comments_hash_strings				as	comments_hash_strings, "
# obsSqlQuery += "os.at_sign_strings						as at_sign_strings, "
# obsSqlQuery += "os.comments_at_strings   				as comments_at_strings, "
# obsSqlQuery += "date_add(session_date, - weekday(session_date)) as session_week_start_day, "
# obsSqlQuery += " vid_raw.obs_session, "
# obsSqlQuery += " vid_raw.media_name, "
# obsSqlQuery += " vid_raw.care, "
# obsSqlQuery += " vid_pct.care_pct*100 AS care_pct, "
# obsSqlQuery += " vid_raw.cut_and_pinch_point, "
# obsSqlQuery += " vid_pct.cut_and_pinch_point_pct*100 AS cut_and_pinch_point_pct, "
# obsSqlQuery += " vid_raw.electrical as electrical, "
# obsSqlQuery += " vid_pct.electrical_pct*100 AS electrical_pct, "
# obsSqlQuery += " vid_raw.engagement, "
# obsSqlQuery += " vid_pct.engagement_pct*100 AS engagement_pct, "
# obsSqlQuery += " vid_raw.falls, "
# obsSqlQuery += " vid_pct.falls_pct*100 AS falls_pct, "
# obsSqlQuery += " vid_raw.fire_protection, "
# obsSqlQuery += " vid_pct.fire_protection_pct*100 AS fire_protection_pct, "
# obsSqlQuery += " vid_raw.hazard, "
# obsSqlQuery += " vid_pct.hazard_pct*100 AS hazard_pct, "
# obsSqlQuery += " vid_raw.housekeeping, "
# obsSqlQuery += " vid_pct.housekeeping_pct*100 AS housekeeping_pct, "
# obsSqlQuery += " vid_raw.ladders, "
# obsSqlQuery += " vid_pct.ladders_pct*100 AS ladders_pct, "
# obsSqlQuery += " vid_raw.lifting, "
# obsSqlQuery += " vid_pct.lifting_pct*100 AS lifting_pct, "
# obsSqlQuery += " vid_raw.overhead_risk, "
# obsSqlQuery += " vid_pct.overhead_risk_pct*100 AS overhead_risk_pct, "
# obsSqlQuery += " vid_raw.planning, "
# obsSqlQuery += " vid_pct.planning_pct*100 AS planning_pct, "
# obsSqlQuery += " vid_raw.ppe, "
# obsSqlQuery += " vid_pct.ppe_pct*100 AS ppe_pct, "
# obsSqlQuery += " vid_raw.tools, "
# obsSqlQuery += " vid_pct.tools_pct*100 AS tools_pct, "
# obsSqlQuery += " vid_raw.weather, "
# obsSqlQuery += " vid_pct.weather_pct*100 AS weather_pct, "
# obsSqlQuery += " vid_raw.total, "
# obsSqlQuery += " vid_pct.total_pct*100 AS total_pct, "
# obsSqlQuery += " osm.url as session_media_url, osm.thumbnail_url as session_media_thumbnail, osm.media_type as session_media_type, "
# obsSqlQuery += " osm.transcript_text as session_media_transcript,"
# obsSqlQuery += " safety_user.user_id, safety_user.email, safety_user.username, "
# obsSqlQuery += " concat_ws(' ', safety_user.first_name, safety_user.middle_name, safety_user.last_name) as user_name, "
# obsSqlQuery += " safety_user.thumbnail_url as user_profile_pic, safety_user.role_name, safety_user.position_name, safety_user.region_name "
# obsSqlQuery += " FROM [db].observation_session as os "
# obsSqlQuery += " LEFT JOIN [db].obs_video_scores_raw as vid_raw ON os.id = vid_raw.obs_session "
# obsSqlQuery += " LEFT JOIN [db].obs_video_scores_pct as vid_pct ON os.id = vid_pct.obs_session   "   
# obsSqlQuery += " LEFT JOIN [db].observation_session_media as osm ON osm.observation_session = os.id AND osm.deleted = 0 "   
# obsSqlQuery += " INNER JOIN [db].party observer_party on os.primary_observer = observer_party.id "
# obsSqlQuery += " AND observer_party.dtype = 'SalesPerson' AND (isnull(observer_party.deleted) OR observer_party.deleted=0) "
# obsSqlQuery += " INNER JOIN [db].person observer ON observer_party.id = observer.party "
# obsSqlQuery += " LEFT JOIN [db].user_position observer_position ON observer_position.id = observer.job_position "
# obsSqlQuery += " INNER JOIN [db].app_user observer_user ON os.primary_observer = observer_user.party "
# obsSqlQuery += "LEFT JOIN [db].person subject  ON os.primary_subject = subject.party "
# obsSqlQuery += "LEFT JOIN [db].user_position subject_position ON subject_position.id = subject.job_position "
# obsSqlQuery += "LEFT JOIN [db].app_user subject_user  ON os.primary_subject = subject_user.party "
# obsSqlQuery += "LEFT JOIN [db].opportunity opportunity   ON os.opportunity = opportunity.id "
# obsSqlQuery += "LEFT JOIN [db].opportunity_record_type as record_type  ON opportunity.opportunity_record_type = record_type.id "
# obsSqlQuery += "LEFT JOIN [db].account account  ON os.account = account.id "
# obsSqlQuery += "INNER JOIN [db].observation ob  ON ob.observation_session = os.id "
# obsSqlQuery += "INNER JOIN [db].phenomenon ph  ON ph.id = ob.phenomenon "
# obsSqlQuery += "INNER JOIN [db].phenomenon_group pg  ON pg.id = ph.phenomenon_group "
# obsSqlQuery += "LEFT JOIN [db].phenomenon_group parent_pg  ON pg.parent = parent_pg.id "
# obsSqlQuery += "LEFT JOIN [db].thing related_thing  ON related_thing.id = os.related_thing_id "
# obsSqlQuery += "LEFT JOIN [db].iot_device  ON iot_device.id = os.iot_device_id "
# obsSqlQuery += "LEFT JOIN [db].party as account_region on  account_region.id = account.region "
# obsSqlQuery += " AND account_region.dtype = 'Region' and account_region.deleted is null "
# obsSqlQuery += "INNER JOIN [db].Safety_Obs_Users AS safety_user ON observer_party.id = safety_user.user_id AND os.zone = safety_user.zone "
# obsSqlQuery += "INNER JOIN [db].zone as zo ON zo.id = os.zone and zo.deleted = 0 "
# obsSqlQuery += " WHERE os.deleted = 0 and os.dtype != 'VideoObservationSession' and os.zone in " + factorlab_zone_ids 

# get_update_fl_data_frame(obsSqlQuery, observationDetailsView)

# COMMAND ----------

# DBTITLE 1,Transcription score data aggregation:
transcriptScoreView = "obs_transcript_score_summary";
transcriptScoreQuery = "SELECT distinct omt.observation_session as session_id, omt.file_name, omt.video_language, omt.transcript_language, omt.transcript, omt.transcription_service, omt.entity_id, omt.entity_type, "
transcriptScoreQuery+="ots_selected.id as selected_id, ots_selected.participant_engagement as participant_engagement_selected, ots_selected.observer_engagement as observer_engagement_selected, "
transcriptScoreQuery+="ots_selected.planning as planning_selected, ots_selected.question_quality as question_quality_selected, ots_selected.real_hazard as real_hazard_selected, "
transcriptScoreQuery+="ots_selected.care care_selected, ots_selected.high_hazard as high_hazard_selected, ots_selected.dim_1, ots_selected.dim_2, ots_selected.cluster_id, ots_selected.model_id, "
transcriptScoreQuery+="ots_selected.last_updated as score_last_updated, ots_low.id as low_id, ots_low.participant_engagement as participant_engagement_low, " 
transcriptScoreQuery+="ots_low.observer_engagement as observer_engagement_low, ots_low.planning as planning_low, ots_low.question_quality as question_quality_low, "
transcriptScoreQuery+="ots_low.real_hazard as real_hazard_low, ots_low.care care_low, ots_low.high_hazard as high_hazard_low, "
transcriptScoreQuery+="ots_med.id as med_id, ots_med.participant_engagement as participant_engagement_med, ots_med.observer_engagement as observer_engagement_med, "
transcriptScoreQuery+="ots_med.planning as planning_med, ots_med.question_quality as question_quality_med, ots_med.real_hazard as real_hazard_med, ots_med.care care_med, "
transcriptScoreQuery+="ots_med.high_hazard as high_hazard_med,	"
transcriptScoreQuery+="ots_high.id as high_id, ots_high.participant_engagement as participant_engagement_high, ots_high.observer_engagement as observer_engagement_high, "
transcriptScoreQuery+="ots_high.planning as planning_high, ots_high.question_quality as question_quality_high, ots_high.real_hazard as real_hazard_high, ots_high.care as "
transcriptScoreQuery+="care_high, ots_high.high_hazard as high_hazard_high, "	 
transcriptScoreQuery+="ots_score.id as score_id, ots_score.participant_engagement as participant_engagement_score, ots_score.observer_engagement as observer_engagement_score, "
transcriptScoreQuery+="ots_score.planning as planning_score, ots_score.question_quality as question_quality_score, ots_score.real_hazard as real_hazard_score, "
transcriptScoreQuery+="ots_score.care as care_score, ots_score.high_hazard as high_hazard_score, "	 
transcriptScoreQuery+="osm.url AS os_media_url, osm.media_type AS os_media_type, osm.thumbnail_url AS os_thumbnail_url, "
transcriptScoreQuery+="ob.url AS obs_media_url, ob.media_type AS obs_media_type, ob.thumbnail_url AS obs_thumbnail_url, "
transcriptScoreQuery+="signal_comment.url AS comment_media_url, signal_comment.media_type AS comment_media_type, signal_comment.thumbnail_url AS comment_thumbnail_url "
transcriptScoreQuery+=" FROM [db].observation_media_transcript omt "
transcriptScoreQuery+="INNER JOIN [db].obs_transcript_model_score ots_selected ON ots_selected.zone = omt.zone AND  ots_selected.obs_session = omt.observation_session AND ots_selected.file_name = omt.file_name and ots_selected.score_type = 'Selected' "
transcriptScoreQuery+="INNER JOIN [db].obs_transcript_model_score ots_low ON ots_low.zone = omt.zone AND  ots_low.obs_session = omt.observation_session AND ots_low.file_name = omt.file_name and ots_low.score_type = 'Low' "
transcriptScoreQuery+="INNER JOIN [db].obs_transcript_model_score ots_med ON ots_med.zone = omt.zone AND  ots_med.obs_session = omt.observation_session AND ots_med.file_name = omt.file_name and ots_med.score_type = 'Medium' "
transcriptScoreQuery+=" INNER JOIN [db].obs_transcript_model_score ots_high ON ots_high.zone = omt.zone AND  ots_high.obs_session = omt.observation_session AND ots_high.file_name = omt.file_name and ots_high.score_type = 'High' "
transcriptScoreQuery+=" INNER JOIN [db].obs_transcript_model_score ots_score ON ots_score.zone = omt.zone AND  ots_score.obs_session = omt.observation_session AND ots_score.file_name = omt.file_name and ots_score.score_type = 'Score' "
transcriptScoreQuery+=" LEFT JOIN [db].observation_session_media osm ON osm.observation_session = omt.entity_id AND omt.entity_type = 'ObservationSession' AND osm.media_type ='VIDEO' "
transcriptScoreQuery+=" LEFT JOIN [db].observation ob ON ob.id = omt.entity_id AND omt.entity_type = 'Observation' AND ob.media_type ='VIDEO' "
transcriptScoreQuery+=" LEFT JOIN [db].app_signal_comment signal_comment ON signal_comment.id = omt.entity_id AND omt.entity_type = 'Comment' AND signal_comment.media_type ='VIDEO' "
transcriptScoreQuery+="WHERE lower(omt.transcription_service) in ('rev', 'aws', 'whisper') AND omt.zone in " + factorlab_zone_ids
get_update_fl_data_frame(transcriptScoreQuery, transcriptScoreView)

# COMMAND ----------

# DBTITLE 1,Observation Details with Transcript Scores:
#Create Observation details with video score View Table:
observationDetailsView = "Safety_Obs_Video_New"
obsSqlQuery = "select os.id as obs_session_id, zo.id as zone, os.dtype as d_type, os.category as category, os.open_issue as open_issue, "
obsSqlQuery += "date_format(os.session_date, 'yyyy-MM-dd')       as  session_date, "
obsSqlQuery += "date_format(os.session_time, 'HH:mm:ss')      as  session_time, "
obsSqlQuery += "case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end as obs_time_zone_off_set, "
obsSqlQuery += "case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone else zo.time_zone end as obs_time_zone, "
# obsSqlQuery += "from_unixtime(unix_timestamp(concat(os.session_date, date_format(os.session_time, ' HH:mm:ss'))) + (case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end)) as session_datetime, "
# obsSqlQuery += "from_utc_timestamp(concat(os.session_date, date_format(os.session_time, ' HH:mm:ss')), case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone else zo.time_zone end) as session_datetime, "
obsSqlQuery += "date_format(from_utc_timestamp(concat(os.session_date, date_format(os.session_time, ' HH:mm:ss')), (case when account.time_zone != null then account.time_zone else zo.time_zone end)), concat('yyyy-MM-dd','\\'','T','\\'','HH:mm:ss')) as session_datetime,"
obsSqlQuery += "concat(os.session_date, date_format(os.session_time, ' HH:mm:ss')) as session_datetime_utc, "
obsSqlQuery += "weekofyear(os.session_date)       as  session_year_week, "
obsSqlQuery += "concat(date_format(os.session_date, 'yyyy'), '-01-01')   as   session_year, "
obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM'), '-01') as session_year_month, "
obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM-dd'), ' ', date_format(os.session_time, 'HH'), ':00:00') as session_year_hour, "
obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM-dd'),' ',date_format(os.session_time, 'HH:mm'), ':00') as session_year_minute, "
# obsSqlQuery += "CAST(from_unixtime(unix_timestamp(os.create_date) + (case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end)) as Timestamp) as obs_create_date,"
# obsSqlQuery += "CAST(from_utc_timestamp(os.create_date, case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone else zo.time_zone end) as Timestamp) as obs_create_date, "
obsSqlQuery += "from_utc_timestamp(os.create_date, (case when account.time_zone != null then account.time_zone else zo.time_zone end)) as obs_create_date,"
obsSqlQuery += "os.is_archive        as is_archive, "
obsSqlQuery += "observer_user.region_id_tokens        as observer_region, "
obsSqlQuery += "observer_user.username                as observer_username, observer_party.entity_state as observer_entity_state,"
obsSqlQuery += "observer_party.id 					  as observer_id, "
obsSqlQuery += "observer_party.time_zone			  as observer_time_zone, "
obsSqlQuery += "observer_party.name					  as observer_name, "
obsSqlQuery += "observer_party.create_date			  as observer_create_date, "
obsSqlQuery += "observer.first_name as observer_first_name, "
obsSqlQuery += "TRIM(concat(IFNULL(observer.middle_name, ''), ' ' , observer.last_name))  as observer_last_name, "
obsSqlQuery += "observer_user.role_name_tokens       as observer_role_names, "
obsSqlQuery += "observer_position.id                 as observer_position_id, "
obsSqlQuery += "observer_position.name               as observer_position_name, "
obsSqlQuery += "subject_user.region_id_tokens        as  subject_region, "
obsSqlQuery += "subject.first_name  as subject_first_name, "
obsSqlQuery += "subject.last_name as subject_last_name, subject.party as subject_id, "
obsSqlQuery += "subject_user.role_name_tokens       as subject_role_names, "
obsSqlQuery += "subject_party.entity_state as subject_user_entity_state, subject_party.dtype as subject_dtype,"
obsSqlQuery += "subject_position.id                 as subject_position_id, "
obsSqlQuery += "subject_position.name               as subject_position_name, "
obsSqlQuery += "opportunity.name as opportunity_name, "
obsSqlQuery += "opportunity.close_date              as  opportunity_close_date, "
obsSqlQuery += "record_type.name as opportunity_record_type_name, "
obsSqlQuery += "account.entity_state as account_entity_state, "
obsSqlQuery += "account.name as       account_name, account.id as account_id, "
obsSqlQuery += "account.time_zone as  account_time_zone, "
obsSqlQuery += "account.location as account_location, "
obsSqlQuery += "account_loc.name as account_location_name, "
obsSqlQuery += "account_region.id					as account_region_id, "
obsSqlQuery += "account_region.name					as account_region_name, "
obsSqlQuery += "os.latitude as latitude, "
obsSqlQuery += "os.longitude as longitude, "
obsSqlQuery += "os.accuracy as location_accuracy, "
obsSqlQuery += "ob.obs_value as       obs_value, "
obsSqlQuery += "ob.comments   as     free_text, "
obsSqlQuery += "ob.url as  ob_url, "
obsSqlQuery += "ob.thumbnail_url as ob_thumbnail_url, "
obsSqlQuery += "ob.media_type as ob_media_type, "
obsSqlQuery += "pg.name as phenomenon_group_name, "
obsSqlQuery += "ph.name as phenomenon_name, "
obsSqlQuery += "ph.statement as phenomenon_statement, "
obsSqlQuery += "ph.dtype as phenomenon_type, "
obsSqlQuery += "ph.units as phenomenon_units, "
obsSqlQuery += "ph.high_end_text as phenomenon_high_end_text, "
obsSqlQuery += "ph.low_end_text as phenomenon_low_end_text, "
obsSqlQuery += "ph.multiple as phenomenon_multiple, "
obsSqlQuery += "ph.auto_fill_key as auto_fill_key, "
obsSqlQuery += "(case "
obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 26  then ph.low_end_text "
obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 51  then 'Two Severity' "
obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 76  then 'Three Severity' "
obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 101 then ph.high_end_text "
obsSqlQuery += " end ) as obs_value_text, "      
obsSqlQuery += "os.comments as  comments, "
obsSqlQuery += "ob.display_value as display_value, "
obsSqlQuery += "ob.answer_feedback as answer_feedback, "
obsSqlQuery += "ob.ph_type as  ph_type, "
obsSqlQuery += "related_thing.name as related_thing_name, "
obsSqlQuery += "related_thing.display_name      as        related_thing_display_name, "
obsSqlQuery += "iot_device.id as   device_id, "
obsSqlQuery += "iot_device.name as  device_name, "
obsSqlQuery += "os.hash_tag_strings 					as	hash_tag_strings, "
obsSqlQuery += "os.comments_hash_strings				as	comments_hash_strings, "
obsSqlQuery += "os.at_sign_strings						as at_sign_strings, "
obsSqlQuery += "os.comments_at_strings   				as comments_at_strings, "
obsSqlQuery += "date_add(session_date, - weekday(session_date)) as session_week_start_day, "
obsSqlQuery += " safety_user.user_id, safety_user.email, safety_user.username, "
obsSqlQuery += " concat_ws(' ', safety_user.first_name, safety_user.middle_name, safety_user.last_name) as user_name, "
obsSqlQuery += " safety_user.thumbnail_url as user_profile_pic, safety_user.role_name, safety_user.position_name, safety_user.region_name, safety_user.region_id, "
obsSqlQuery += " osm.url as session_media_url, osm.thumbnail_url as session_media_thumbnail, osm.media_type as session_media_type, "
obsSqlQuery += " osm.transcript_text as session_media_transcript,"
obsSqlQuery += " media_score.* "
obsSqlQuery += " FROM [db].observation_session as os "
obsSqlQuery += " LEFT JOIN [db].obs_transcript_score_summary AS media_score ON os.id = media_score.session_id " 
obsSqlQuery += " LEFT JOIN [db].observation_session_media as osm ON osm.observation_session = os.id AND osm.deleted = 0 "   
obsSqlQuery += "INNER JOIN [db].party observer_party on os.primary_observer = observer_party.id "
obsSqlQuery += "AND observer_party.dtype = 'SalesPerson' AND observer_party.entity_state != 'DELETE' "
obsSqlQuery += "INNER JOIN [db].app_user observer_user ON observer_party.id = observer_user.party "
obsSqlQuery += "LEFT JOIN [db].person observer ON observer_party.id = observer.party "
obsSqlQuery += "LEFT JOIN [db].user_position observer_position ON observer_position.id = observer.job_position "
obsSqlQuery += "LEFT JOIN [db].person subject ON os.primary_subject = subject.party "
obsSqlQuery += "LEFT JOIN [db].user_position subject_position ON subject_position.id = subject.job_position "
obsSqlQuery += "LEFT JOIN [db].party subject_party  ON subject_party.id = os.primary_subject "
obsSqlQuery += "LEFT JOIN [db].app_user subject_user  ON subject_party.id = subject_user.party "
obsSqlQuery += "LEFT JOIN [db].opportunity opportunity   ON os.opportunity = opportunity.id "
obsSqlQuery += "LEFT JOIN [db].opportunity_record_type as record_type  ON opportunity.opportunity_record_type = record_type.id "
obsSqlQuery += "LEFT JOIN [db].account account  ON os.account = account.id AND (account.entity_state is null OR account.entity_state != 'DELETE') "
obsSqlQuery += "LEFT JOIN [db].account_location account_loc  ON account.location = account_loc.id AND account.zone=account_loc.zone "
obsSqlQuery += "INNER JOIN [db].observation ob  ON ob.observation_session = os.id and ob.deleted = 0 "
obsSqlQuery += "INNER JOIN [db].phenomenon ph  ON ph.id = ob.phenomenon "
obsSqlQuery += "INNER JOIN [db].phenomenon_group pg  ON pg.id = ph.phenomenon_group "
obsSqlQuery += "LEFT JOIN [db].phenomenon_group parent_pg  ON pg.parent = parent_pg.id "
obsSqlQuery += "LEFT JOIN [db].thing related_thing  ON related_thing.id = os.related_thing_id "
obsSqlQuery += "LEFT JOIN [db].iot_device  ON iot_device.id = os.iot_device_id "
obsSqlQuery += "LEFT JOIN [db].party as account_region on  account_region.id = account.region "
obsSqlQuery += "AND account_region.dtype = 'Region' and account_region.deleted is null "
obsSqlQuery += "INNER JOIN [db].Safety_Obs_Users AS safety_user ON observer_party.id = safety_user.user_id AND os.zone = safety_user.zone "
obsSqlQuery += "INNER JOIN [db].zone as zo ON zo.id = os.zone and zo.deleted = 0 "
obsSqlQuery += "WHERE os.deleted = 0 and os.dtype != 'VideoObservationSession' and os.zone in " + factorlab_zone_ids 
# print(obsSqlQuery)
get_update_fl_data_frame(obsSqlQuery, observationDetailsView)

# COMMAND ----------

# #Create Observation details with video score View Table:
# #schemaName = "";
# observationDetailsView = "Obs_Raw_WithAccMapping"

# obsSqlQuery = ""
# obsSqlQuery += " select os.id as obs_session_id, zo.accessor_zone as zone, "
# obsSqlQuery += " os.dtype as d_type, os.category as category, os.open_issue as open_issue,  "
# obsSqlQuery += " date_format(os.session_date, 'yyyy-MM-dd')       as  session_date,  "
# obsSqlQuery += " date_format(os.session_time, 'HH:mm:ss')      as  session_time,  "
# obsSqlQuery += " case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end as obs_time_zone_off_set,  "
# obsSqlQuery += " case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone else zo.time_zone end as obs_time_zone,  "
# obsSqlQuery += " from_unixtime(unix_timestamp(concat(os.session_date, date_format(os.session_time, ' HH:mm:ss'))) + (case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end)) as session_datetime,  "
# obsSqlQuery += " concat(os.session_date, date_format(os.session_time, ' HH:mm:ss')) as session_datetime_utc,  "
# obsSqlQuery += " weekofyear(os.session_date)       as  session_year_week,  "
# obsSqlQuery += " concat(date_format(os.session_date, 'yyyy'), '-01-01')   as   session_year,  "
# obsSqlQuery += " concat(date_format(os.session_date, 'yyyy-MM'), '-01') as session_year_month,  "
# obsSqlQuery += " date_add(session_date, - weekday(session_date)) as session_week_start_day,  "
# obsSqlQuery += " concat(date_format(os.session_date, 'yyyy-MM-dd'), ' ', date_format(os.session_time, 'HH'), ':00:00') as session_year_hour,  "
# obsSqlQuery += " concat(date_format(os.session_date, 'yyyy-MM-dd'),' ',date_format(os.session_time, 'HH:mm'), ':00') as session_year_minute,  "
# obsSqlQuery += " CAST(from_unixtime(unix_timestamp(os.create_date) + (case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end)) as Timestamp) as obs_create_date, "
# obsSqlQuery += " os.latitude as latitude,  "
# obsSqlQuery += " os.longitude as longitude,  "
# obsSqlQuery += " os.accuracy as location_accuracy,  "
# obsSqlQuery += " -- account group -- "
# obsSqlQuery += " account.accessor_id as account_id, "
# obsSqlQuery += " account.entity_state as account_entity_state,  "
# obsSqlQuery += " account.name as account_name,  "
# obsSqlQuery += " account.time_zone as account_time_zone,  "
# obsSqlQuery += " account.region_id as account_region_id,  "
# obsSqlQuery += " account.region_name as account_region_name,  "
# obsSqlQuery += " account.location_id as account_location,  "
# obsSqlQuery += " account.location_name as account_location_name,  "
# obsSqlQuery += " -- primary observer group -- "
# obsSqlQuery += " observer.accessor_id as observer_id,  "
# obsSqlQuery += " observer.entity_state as observer_entity_state, "
# obsSqlQuery += " observer.name as observer_name,  "
# obsSqlQuery += " observer.first_name as observer_first_name,  "
# obsSqlQuery += " observer.last_name as observer_last_name,  "
# obsSqlQuery += " observer.username as observer_username,  "
# obsSqlQuery += " observer.time_zone as observer_time_zone,  "
# obsSqlQuery += " observer.create_date as observer_create_date,  "
# obsSqlQuery += " observer.region_id_tokens as observer_region,  "
# obsSqlQuery += " observer.role_name_tokens as observer_role_names,  "
# obsSqlQuery += " observer.position_id as observer_position_id,  "
# obsSqlQuery += " observer.position_name as observer_position_name,  "
# obsSqlQuery += " safety_user.user_id,  "
# obsSqlQuery += " safety_user.email,  "
# obsSqlQuery += " safety_user.username,  "
# obsSqlQuery += " concat_ws(' ', safety_user.first_name, safety_user.middle_name, safety_user.last_name) as user_name,  "
# obsSqlQuery += " safety_user.thumbnail_url as user_profile_pic,  "
# obsSqlQuery += " safety_user.role_name,  "
# obsSqlQuery += " safety_user.position_name,  "
# obsSqlQuery += " safety_user.region_name,  "
# obsSqlQuery += " safety_user.region_id,  "
# obsSqlQuery += " -- subject group -- "
# obsSqlQuery += " subject.accessor_id as subject_id,  "
# obsSqlQuery += " subject.dtype as subject_dtype, "
# obsSqlQuery += " subject.entity_state as subject_user_entity_state,  "
# obsSqlQuery += " subject.first_name  as subject_first_name,  "
# obsSqlQuery += " subject.last_name as subject_last_name,  "
# obsSqlQuery += " subject.role_name_tokens       as subject_role_names,  "
# obsSqlQuery += " subject.region_id_tokens        as  subject_region,  "
# obsSqlQuery += " subject.position_id                 as subject_position_id,  "
# obsSqlQuery += " subject.position_name               as subject_position_name,  "
# obsSqlQuery += " -- contractor group -- "
# obsSqlQuery += " observer_contractor.accessor_id as observer_contractor_id,  "
# obsSqlQuery += " observer_contractor.name        as observer_contractor_name,  "
# obsSqlQuery += " subject_contractor.accessor_id as subject_contractor_id,  "
# obsSqlQuery += " subject_contractor.name        as subject_contractor_name,  "
# obsSqlQuery += " -- observation group -- "
# obsSqlQuery += " ob.obs_value as       obs_value,  "
# obsSqlQuery += " ob.comments   as     free_text,  "
# obsSqlQuery += " ob.url as  ob_url,  "
# obsSqlQuery += " ob.thumbnail_url as ob_thumbnail_url,  "
# obsSqlQuery += " ob.media_type as ob_media_type,  "
# obsSqlQuery += " pg.name as phenomenon_group_name,  "
# obsSqlQuery += " ph.name as phenomenon_name,  "
# obsSqlQuery += " ph.statement as phenomenon_statement,  "
# obsSqlQuery += " ph.dtype as phenomenon_type,  "
# obsSqlQuery += " ph.units as phenomenon_units,  "
# obsSqlQuery += " ph.high_end_text as phenomenon_high_end_text,  "
# obsSqlQuery += " ph.low_end_text as phenomenon_low_end_text,  "
# obsSqlQuery += " ph.multiple as phenomenon_multiple,  "
# obsSqlQuery += " ph.auto_fill_key as auto_fill_key,  "
# obsSqlQuery += " (case  "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 26  then ph.low_end_text "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 51  then 'Two Severity'  "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 76  then 'Three Severity'  "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 101 then ph.high_end_text  "
# obsSqlQuery += " end ) as obs_value_text,      "
# obsSqlQuery += " ob.display_value as display_value,    "
# obsSqlQuery += " ob.answer_feedback as answer_feedback,  "
# obsSqlQuery += " ob.ph_type as  ph_type,  "
# obsSqlQuery += " os.comments as  comments,  "
# obsSqlQuery += " os.hash_tag_strings 					as	hash_tag_strings,  "
# obsSqlQuery += " os.comments_hash_strings				as	comments_hash_strings,  "
# obsSqlQuery += " os.at_sign_strings						as at_sign_strings,  "
# obsSqlQuery += " os.comments_at_strings   				as comments_at_strings,  "
# obsSqlQuery += " record_type.name as opportunity_record_type_name,  "
# obsSqlQuery += " opportunity.name as opportunity_name,  "
# obsSqlQuery += " opportunity.close_date              as  opportunity_close_date,  "
# obsSqlQuery += " related_thing.name as related_thing_name,  "
# obsSqlQuery += " related_thing.display_name      as        related_thing_display_name,  "
# obsSqlQuery += " iot_device.id as   device_id,  "
# obsSqlQuery += " iot_device.name as  device_name,  "
# obsSqlQuery += " osm.url as session_media_url, osm.thumbnail_url as session_media_thumbnail, osm.media_type as session_media_type,  "
# obsSqlQuery += " osm.transcript_text as session_media_transcript, "
# obsSqlQuery += " media_score.*   "
# obsSqlQuery += " FROM default.observation_session as os  "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " INNER JOIN test.fed_zone_view as zo ON zo.provider_zone = os.zone -- and zo.deleted = 0   "
# obsSqlQuery += " -- account group -- "
# obsSqlQuery += " LEFT JOIN test.fed_account_view account ON account.provider_zone=zo.provider_zone and account.accessor_zone=zo.accessor_zone and account.provider_id=os.account  "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " -- observer group -- "
# obsSqlQuery += " inner join test.fed_observer_view observer ON observer.provider_zone=zo.provider_zone and observer.accessor_zone=zo.accessor_zone and observer.provider_id = os.primary_observer "
# obsSqlQuery += " INNER JOIN default.Safety_Obs_Users AS safety_user ON os.primary_observer = safety_user.user_id AND safety_user.zone = os.zone   "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " -- subject group -- "
# obsSqlQuery += " left join test.fed_subject_view subject ON subject.provider_zone=zo.provider_zone and subject.accessor_zone=zo.accessor_zone and subject.provider_id = os.primary_subject "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " -- contractor group -- "
# obsSqlQuery += " left join test.fed_contractor_view observer_contractor ON observer_contractor.provider_zone=zo.provider_zone and observer_contractor.accessor_zone=zo.accessor_zone and observer_contractor.provider_id = os.observer_contractor "
# obsSqlQuery += " left join test.fed_contractor_view subject_contractor  ON subject_contractor.provider_zone=zo.provider_zone and subject_contractor.accessor_zone=zo.accessor_zone and subject_contractor.provider_id = os.subject_contractor "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " -- pg group -- "
# obsSqlQuery += " INNER JOIN default.observation ob  ON ob.observation_session = os.id and ob.deleted = 0  "
# obsSqlQuery += " INNER JOIN default.phenomenon ph  ON ph.id = ob.phenomenon  "
# obsSqlQuery += " INNER JOIN default.phenomenon_group pg  ON pg.id = ph.phenomenon_group  "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " -- opportunity group -- "
# obsSqlQuery += " LEFT JOIN default.opportunity opportunity   ON os.opportunity = opportunity.id  "
# obsSqlQuery += " LEFT JOIN default.opportunity_record_type as record_type  ON opportunity.opportunity_record_type = record_type.id  "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " -- thing group -- "
# obsSqlQuery += " LEFT JOIN default.thing related_thing  ON related_thing.id = os.related_thing_id  "
# obsSqlQuery += " LEFT JOIN default.iot_device  ON iot_device.id = os.iot_device_id  "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " -- transcription group -- "
# obsSqlQuery += " LEFT JOIN default.obs_transcript_score_summary AS media_score ON os.id = media_score.session_id   "
# obsSqlQuery += " LEFT JOIN default.observation_session_media as osm ON osm.observation_session = os.id AND osm.deleted = 0  "
# obsSqlQuery += " -- ------------------------------------ "
# obsSqlQuery += " WHERE os.deleted = 0 and os.dtype != 'VideoObservationSession' and zo.id in " +  factorlab_zone_ids

# get_update_fl_data_frame(obsSqlQuery, observationDetailsView)

# COMMAND ----------

# DBTITLE 1,Observation Details with Transcript Scores and Surge Awards
# #Create Observation details with video score filter by surge awards View Table:
# observationDetailsSurgeView = "Safety_Obs_Video_New_Surge"
# obsSqlQuery = "select os.id as obs_session_id, zo.id as zone, zo.name as zone_name, os.dtype as d_type, os.category as category, os.open_issue as open_issue, "
# obsSqlQuery += "date_format(os.session_date, 'yyyy-MM-dd')       as  session_date, "
# obsSqlQuery += "date_format(os.session_time, 'HH:mm:ss')      as  session_time, "
# obsSqlQuery += "case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end as obs_time_zone_off_set, "
# obsSqlQuery += "case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone else zo.time_zone end as obs_time_zone, "
# obsSqlQuery += "from_unixtime(unix_timestamp(concat(os.session_date, date_format(os.session_time, ' HH:mm:ss'))) + (case when CAST(account.time_zone_off_set as int) > 0 then account.time_zone_off_set else zo.time_zone_off_set end)) as session_datetime, "
# obsSqlQuery += "concat(os.session_date, date_format(os.session_time, ' HH:mm:ss')) as session_datetime_utc, "
# obsSqlQuery += "weekofyear(os.session_date)       as  session_year_week, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy'), '-01-01')   as   session_year, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM'), '-01') as session_year_month, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM-dd'), ' ', date_format(os.session_time, 'HH'), ':00:00') as session_year_hour, "
# obsSqlQuery += "concat(date_format(os.session_date, 'yyyy-MM-dd'),' ',date_format(os.session_time, 'HH:mm'), ':00') as session_year_minute, "
# obsSqlQuery += "observer_user.region_id_tokens        as observer_region, "
# obsSqlQuery += "observer_user.username                as observer_username, observer_party.entity_state as observer_entity_state,"
# obsSqlQuery += "observer_party.id 					  as observer_id, "
# obsSqlQuery += "observer_party.time_zone			  as observer_time_zone, "
# obsSqlQuery += "observer_party.name					  as observer_name, "
# obsSqlQuery += "observer_party.create_date			  as observer_create_date, "
# obsSqlQuery += "observer.first_name as observer_first_name, "
# obsSqlQuery += "TRIM(concat(IFNULL(observer.middle_name, ''), ' ' , observer.last_name))  as observer_last_name, "
# obsSqlQuery += "observer_user.role_name_tokens       as observer_role_names, "
# obsSqlQuery += "observer_position.id                 as observer_position_id, "
# obsSqlQuery += "observer_position.name               as observer_position_name, "
# obsSqlQuery += "subject_user.region_id_tokens        as  subject_region, "
# obsSqlQuery += "subject.first_name  as subject_first_name, "
# obsSqlQuery += "subject.last_name as subject_last_name, subject.party as subject_id, "
# obsSqlQuery += "subject_user.role_name_tokens       as subject_role_names, "
# obsSqlQuery += "subject_party.entity_state as subject_user_entity_state, "
# obsSqlQuery += "subject_position.id                 as subject_position_id, "
# obsSqlQuery += "subject_position.name               as subject_position_name, "
# obsSqlQuery += "opportunity.name as opportunity_name, "
# obsSqlQuery += "opportunity.close_date              as  opportunity_close_date, "
# obsSqlQuery += "record_type.name as opportunity_record_type_name, "
# obsSqlQuery += "account.entity_state as account_entity_state, "
# obsSqlQuery += "account.name as       account_name, account.id as account_id, "
# obsSqlQuery += "account.time_zone as  account_time_zone, "
# obsSqlQuery += "account.location as account_location, "
# obsSqlQuery += "account_region.id					as account_region_id, "
# obsSqlQuery += "account_region.name					as account_region_name, "
# obsSqlQuery += "os.latitude as latitude, "
# obsSqlQuery += "os.longitude as longitude, "
# obsSqlQuery += "os.accuracy as location_accuracy, "
# obsSqlQuery += "ob.obs_value as       obs_value, "
# obsSqlQuery += "ob.comments   as     free_text, "
# obsSqlQuery += "ob.url as  ob_url, "
# obsSqlQuery += "ob.thumbnail_url as ob_thumbnail_url, "
# obsSqlQuery += "ob.media_type as ob_media_type, "
# obsSqlQuery += "pg.name as phenomenon_group_name, "
# obsSqlQuery += "ph.name as phenomenon_name, "
# obsSqlQuery += "ph.statement as phenomenon_statement, "
# obsSqlQuery += "ph.dtype as phenomenon_type, "
# obsSqlQuery += "ph.units as phenomenon_units, "
# obsSqlQuery += "ph.high_end_text as phenomenon_high_end_text, "
# obsSqlQuery += "ph.low_end_text as phenomenon_low_end_text, "
# obsSqlQuery += "ph.multiple as phenomenon_multiple, "
# obsSqlQuery += "ph.auto_fill_key as auto_fill_key, "
# obsSqlQuery += "(case "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 26  then ph.low_end_text "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 51  then 'Two Severity' "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 76  then 'Three Severity' "
# obsSqlQuery += " when round(coalesce(ob.obs_value,0)*100*coalesce(ph.min_value,0)/coalesce(ph.max_value,1)) < 101 then ph.high_end_text "
# obsSqlQuery += " end ) as obs_value_text, "      
# obsSqlQuery += "os.comments as  comments, "
# obsSqlQuery += "ob.display_value as display_value, "
# obsSqlQuery += "ob.answer_feedback as answer_feedback, "
# obsSqlQuery += "ob.ph_type as  ph_type, "
# obsSqlQuery += "related_thing.name as related_thing_name, "
# obsSqlQuery += "related_thing.display_name      as        related_thing_display_name, "
# obsSqlQuery += "iot_device.id as   device_id, "
# obsSqlQuery += "iot_device.name as  device_name, "
# obsSqlQuery += "os.hash_tag_strings 					as	hash_tag_strings, "
# obsSqlQuery += "os.comments_hash_strings				as	comments_hash_strings, "
# obsSqlQuery += "os.at_sign_strings						as at_sign_strings, "
# obsSqlQuery += "os.comments_at_strings   				as comments_at_strings, "
# obsSqlQuery += "date_add(session_date, - weekday(session_date)) as session_week_start_day, "
# obsSqlQuery += " safety_user.user_id, safety_user.email, safety_user.username, "
# obsSqlQuery += " concat_ws(' ', safety_user.first_name, safety_user.middle_name, safety_user.last_name) as user_name, "
# obsSqlQuery += " safety_user.thumbnail_url as user_profile_pic, safety_user.role_name, safety_user.position_name, safety_user.region_name, safety_user.region_id, "
# obsSqlQuery += " osm.url as session_media_url, osm.thumbnail_url as session_media_thumbnail, osm.media_type as session_media_type, "
# obsSqlQuery += " osm.transcript_text as session_media_transcript,"
# obsSqlQuery += " media_score.* "
# obsSqlQuery += " FROM [db].observation_session as os "
# obsSqlQuery += " LEFT JOIN [db].obs_transcript_score_summary AS media_score ON os.id = media_score.session_id " 
# obsSqlQuery += " LEFT JOIN [db].observation_session_media as osm ON osm.observation_session = os.id AND osm.deleted = 0 "   
# obsSqlQuery += "INNER JOIN [db].party observer_party on os.primary_observer = observer_party.id "
# obsSqlQuery += "AND observer_party.dtype = 'SalesPerson' AND observer_party.entity_state != 'DELETE' "
# obsSqlQuery += "INNER JOIN [db].app_user observer_user ON observer_party.id = observer_user.party "
# obsSqlQuery += "LEFT JOIN [db].person observer ON observer_party.id = observer.party "
# obsSqlQuery += "LEFT JOIN [db].user_position observer_position ON observer_position.id = observer.job_position "
# obsSqlQuery += "LEFT JOIN [db].person subject ON os.primary_subject = subject.party "
# obsSqlQuery += "LEFT JOIN [db].user_position subject_position ON subject_position.id = subject.job_position "
# obsSqlQuery += "LEFT JOIN [db].app_user subject_user  ON os.primary_subject = subject_user.party "
# obsSqlQuery += "LEFT JOIN [db].party subject_party  ON subject_party.id = subject_user.party "
# obsSqlQuery += "LEFT JOIN [db].opportunity opportunity   ON os.opportunity = opportunity.id "
# obsSqlQuery += "LEFT JOIN [db].opportunity_record_type as record_type  ON opportunity.opportunity_record_type = record_type.id "
# obsSqlQuery += "LEFT JOIN [db].account account  ON os.account = account.id AND (account.entity_state is null OR account.entity_state != 'DELETE') "
# obsSqlQuery += "INNER JOIN [db].observation ob  ON ob.observation_session = os.id "
# obsSqlQuery += "INNER JOIN [db].phenomenon ph  ON ph.id = ob.phenomenon "
# obsSqlQuery += "INNER JOIN [db].phenomenon_group pg  ON pg.id = ph.phenomenon_group "
# obsSqlQuery += "LEFT JOIN [db].phenomenon_group parent_pg  ON pg.parent = parent_pg.id "
# obsSqlQuery += "LEFT JOIN [db].thing related_thing  ON related_thing.id = os.related_thing_id "
# obsSqlQuery += "LEFT JOIN [db].iot_device  ON iot_device.id = os.iot_device_id "
# obsSqlQuery += "LEFT JOIN [db].party as account_region on  account_region.id = account.region "
# obsSqlQuery += "AND account_region.dtype = 'Region' and account_region.deleted is null "
# obsSqlQuery += "INNER JOIN [db].Safety_Obs_Users AS safety_user ON observer_party.id = safety_user.user_id AND os.zone = safety_user.zone "
# obsSqlQuery += "INNER JOIN [db].zone as zo ON zo.id = os.zone and zo.deleted = 0 "
# obsSqlQuery += "WHERE os.deleted = 0 and os.dtype != 'VideoObservationSession' " 
# obsSqlQuery += " and ((os.hash_tag_strings like '%#surgemost%') or (os.comments_hash_strings like '%#surgemost%') or (os.hash_tag_strings like '%#surgebest%') or (os.comments_hash_strings like '%#surgebest%') or (os.zone = 20321))" 

# get_update_fl_data_frame(obsSqlQuery, observationDetailsSurgeView)

# COMMAND ----------

# DBTITLE 1,Video Feedback Observation:
#Create Observation details with video score View Table:
videoFeedbackView = "video_observation_feedback"
feedbackSqlQuery="select foo.*, feedback_user.id feedback_user_id, feedback_user.name feedback_user_name, omt.file_name, omt.thumbnail_url, omt.transcript, "
feedbackSqlQuery+=" os.id session_id, os.session_date session_date, os.session_time session_time, os_observer.id observer_id, os_observer.name observer_name, " 
feedbackSqlQuery+=" os_account.id project_id, os_account.name project_name, os_subject.id subject_id, os_subject.name subject_name from ( "
feedbackSqlQuery+=" select os.id as vid_session_id, os.zone as zone, os.session_date as vid_session_date, os.session_time as vid_session_time, " 
feedbackSqlQuery+=" os.source_session_id, os.primary_observer as vid_obs_user, os.video_name, pg.name as pg_name, " 
feedbackSqlQuery+=" sum(if(ph.auto_fill_key = 'care', ob.display_value, 0)) as care, sum(if(ph.auto_fill_key = 'high_hazard', ob.display_value, 0)) as high_hazard, "
feedbackSqlQuery+=" sum(if(ph.auto_fill_key = 'observer_engagement', ob.display_value, 0)) as observer_engagement, " 
feedbackSqlQuery+=" sum(if(ph.auto_fill_key = 'participant_engagement', ob.display_value, 0)) as participant_engagement, "
feedbackSqlQuery+=" sum(if(ph.auto_fill_key = 'planning', ob.display_value, 0)) as planning, "
feedbackSqlQuery+=" sum(if(ph.auto_fill_key = 'question_quality', ob.display_value, 0)) as question_quality, "
feedbackSqlQuery+=" sum(if(ph.auto_fill_key = 'real_hazard', ob.display_value, 0)) as real_hazard "
feedbackSqlQuery+=" from [db].observation_session os inner join [db].observation ob  on ob.observation_session = os.id "
feedbackSqlQuery+=" inner join [db].phenomenon ph on ph.id = ob.phenomenon inner join phenomenon_group pg on pg.id = ph.phenomenon_group "
feedbackSqlQuery+=" where os.dtype = 'VideoObservationSession' and pg.video_feedback_flg = 1 and pg.display_screen = 'FeedbackReport' " 
feedbackSqlQuery+=" and os.deleted = 0 group by os.id, os.zone, os.session_date, os.session_time, os.source_session_id, os.primary_observer, " 
feedbackSqlQuery+=" os.video_name, pg.name) as foo "
feedbackSqlQuery+=" inner join [db].party feedback_user on feedback_user.id = foo.vid_obs_user and (isnull(feedback_user.deleted) OR feedback_user.deleted=0) "
feedbackSqlQuery+=" left join [db].observation_media_transcript omt on omt.zone = foo.zone and omt.observation_session = foo.source_session_id " 
feedbackSqlQuery+=" and omt.file_name = foo.video_name inner join [db].observation_session os on os.zone = foo.zone and os.id = foo.source_session_id "
feedbackSqlQuery+=" inner join [db].party os_observer on os_observer.id = os.primary_observer and (isnull(os_observer.deleted) OR os_observer.deleted=0) "
feedbackSqlQuery+=" left join [db].account os_account on os_account.id = os.account and os_account.deleted = 0 "
feedbackSqlQuery+=" left join [db].party os_subject  on os_subject.id = os.primary_subject and (isnull(os_subject.deleted) OR os_subject.deleted=0) "

get_update_fl_data_frame(feedbackSqlQuery, videoFeedbackView)

# COMMAND ----------

# DBTITLE 1,Delete and Create User activity Records Table:
#Create User Activity Records View Table:
userActivityView = "User_Activity_Records"
userActivitySqlQuery = "SELECT activity.id as id, app_user.username as username, observer.first_name  as observer_first_name, "
userActivitySqlQuery += "TRIM(concat(coalesce(observer.middle_name, ''), ' ', observer.last_name)) as observer_last_name, "
userActivitySqlQuery += "app_user.region_id_tokens as observer_region, position.name as observer_job_position, "
userActivitySqlQuery += "activity.month_date as month_date, activity.record_date as record_date, activity.max_count as count, "
userActivitySqlQuery += "activity.activity_type as activity_type, activity.type as type, zo.id	 as zone, activity.user	as observer_id, "
userActivitySqlQuery += "activity.user as subject_id, 1 as shared, foo.activity_count as maxRecord "
userActivitySqlQuery += "FROM [db].user_activity_log as activity "
userActivitySqlQuery += "INNER JOIN [db].zone zo ON zo.id = activity.zone AND zo.deleted = 0 "
userActivitySqlQuery += "INNER JOIN [db].app_user app_user ON activity.user = app_user.party "
userActivitySqlQuery += "AND (isnull(app_user.deleted_token) OR app_user.deleted_token = '') "
userActivitySqlQuery += "INNER JOIN [db].person observer ON app_user.party = observer.party "
userActivitySqlQuery += "LEFT JOIN [db].user_position position ON position.id = observer.job_position "
userActivitySqlQuery += "LEFT JOIN  (SELECT ual.user AS party_id, ual.type, ual.activity_type, MAX(max_count) AS activity_count "
userActivitySqlQuery += "FROM [db].user_activity_log ual GROUP BY ual.user,ual.type,ual.activity_type) AS foo "
userActivitySqlQuery += "ON observer.party = foo.party_id AND activity.type = foo.type AND activity.activity_type = foo.activity_type "
userActivitySqlQuery += "WHERE activity.zone in " + factorlab_zone_ids

get_update_fl_data_frame(userActivitySqlQuery, userActivityView)

# COMMAND ----------

# DBTITLE 1,Observation Media Transcript View:
transcript_view = "media_observation_transcription_view"
trancript_sql = "SELECT z.id as zone_id, z.name as zone_name, os. id as session_id, date_format(os.create_date, 'yyyy-MM-dd HH:mm:ss') as session_date_time_UTC, "
trancript_sql+= "fl_user.id as observer_id, fl_user.name as observer_name, fl_acc.id as project_id, fl_acc.name as project_name, fl_sub.id as subject_id, "
trancript_sql+= "fl_sub.name as subject_name, a.entity_type, a.entity_id, a.file_name, concat(a.zone, '/videos/', a.file_name)  as s3_key, " 
trancript_sql+= "'6e9607cf-034c-42bb-9fce-3a7019c7f95c' as s3_bucket,  b.duration, b.file_size, b.media_format, a.video_language, a.transcript_language, " 
trancript_sql+= "a.transcription_service, a.transcript, c.transcript as translation, date_format(a.last_updated, 'yyyy-MM-dd HH:mm:ss') as transcript_last_updated " 
trancript_sql+= "FROM [db].observation_media_transcript a INNER JOIN zone z on z.id=a.zone and z.deleted=0 "
trancript_sql+= "INNER JOIN [db].observation_session os on a.zone = os.zone and os.id = a.observation_session "
trancript_sql+= "LEFT JOIN [db].observation_media_details b on a.zone=b.zone and a.file_name=b.file_name "
trancript_sql+= "LEFT JOIN [db].observation_media_transcript c on c.zone=a.zone and c.file_name=a.file_name and lower(c.video_language) !='english' " 
trancript_sql+= "and lower(c.transcript_language) = 'english' and lower(c.transcription_service) in ('aws') "
trancript_sql+= "LEFT JOIN [db].party fl_user on fl_user.id=os.primary_observer and lower(fl_user.dtype)= 'salesperson' "
trancript_sql+= "LEFT JOIN [db].account fl_acc on fl_acc.id = os.account "
trancript_sql+= "LEFT JOIN [db].party fl_sub on fl_sub.id=os.primary_subject and lower(fl_sub.dtype) in ('salesperson', 'contractor') "
trancript_sql+= "WHERE lower(a.transcription_service) in ('rev', 'happy', 'whisper') and lower(a.transcript_status) in ('success', 'complete') and lower(a.transcript_language) = 'spanish' "

get_update_fl_data_frame(trancript_sql, transcript_view)

# COMMAND ----------

# DBTITLE 1,Observation User's Media:
obs_media_view = "obs_media_by_user"
obs_media_sql ="SELECT foo.session_id, foo.type, foo.zone, date_format(foo.create_date, 'yyyy-MM-dd HH:mm:ss') as create_date, foo.url as media_name, lower(foo.media_type) as media_type, foo.media_owner from ( "
obs_media_sql+= "SELECT os.id as session_id, 'Session' as type, os.zone, os.create_date, osm.url, osm.media_type , os.primary_observer as media_owner from "
obs_media_sql+= "[db].observation_session os inner join [db].observation_session_media osm on osm.observation_session = os.id where os.deleted =0 and osm.deleted =0 and lower(osm.media_type) = 'video' " 
obs_media_sql+= "and osm.media_type is not null UNION ALL " 
obs_media_sql+= "SELECT os.id as session_id,'Observation' as type,os.zone,os.create_date, osm.url, osm.media_type, os.primary_observer as media_owner from [db].observation_session os " 
obs_media_sql+= "inner join [db].observation osm on osm.observation_session = os.id where os.deleted =0 and osm.deleted =0 and lower(osm.media_type) = 'video' and osm.media_type is not null "
obs_media_sql+= "UNION ALL "
obs_media_sql+= "SELECT srr.obs_session_id as session_id, 'Comment' as type, srr.zone, acm.last_updated as create_date, acm.url, acm.media_type, acm.posted_by as media_owner " 
obs_media_sql+= "from [db].app_signal_comment acm inner join [db].signal_related_records srr on srr.app_signal = acm.app_signal where srr.deleted=0 and lower(acm.media_type) = 'video' " 
obs_media_sql+= " and acm.media_type is not null ) as foo "

get_update_fl_data_frame(obs_media_sql, obs_media_view)

# COMMAND ----------

# DBTITLE 1,Observation video meta data with activity/state:
#for permissions: GRANT EXECUTE ON factorlab.* TO 'databricks_user'@'%';
# from contextlib import contextmanager
# import pymysql
# import sys

# @contextmanager
# def get_connection():
#     connection = pymysql.connect(host=jdbcHostname,  user=jdbcUser, password=jdbcPass, db=jdbcDatabase, charset='utf8mb4')
#     try:
#         yield connection
#     except Exception as e:
#       print("ERROR-Connection : " + str(e))
#       raise Exception(str(e))
#     finally:
#         connection.close()

# try:
#   for zone_id in watch_zone_ids:
#     with get_connection() as con:
#       with con.cursor() as cursor:
#         cur = con.cursor()
#         try:
#           print(zone_id)
#           obs_video_meta_data_table = "obs_videos_meta_data_" 
  
#           cur.execute("call get_obs_video_meta_data({0}, '');".format(zone_id))
#           df_fl = spark.read.jdbc(url=jdbcUrl, table = obs_video_meta_data_table + "temp")
#           df_fl.write.format("delta").option("overwriteSchema", "true").mode("overwrite").saveAsTable(schemaName + obs_video_meta_data_table + str(zone_id))
          
#           cur.execute("call get_obs_video_meta_data({0}, '_ar');".format(zone_id))
#           df_fl = spark.read.jdbc(url=jdbcUrl, table = obs_video_meta_data_table + "temp")
#           df_fl.write.format("delta").option("overwriteSchema", "true").mode("append").saveAsTable(schemaName + obs_video_meta_data_table + str(zone_id))
#         except Exception as e:
#           print("skipped : " + str(e))
# except Exception as e:
#   print("Error : " + str(e))


# COMMAND ----------

import datetime as dt
print("FL-data updated successfully in: ", (dt.datetime.today()-start_time));

# COMMAND ----------

